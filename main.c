#include <stdio.h>
#include <locale.h>

void selectionSort(int v[], int n) {
    int maior, aux;

    for (int i = 0; i < (n - 1); i++) {
        maior = i;
        for (int j = (i + 1); j < n; j++) {
            if (v[j] < v[maior])
                maior = j;
        }

        if (v[i] != v[maior]) {
            aux = v[i];
            v[i] = v[maior];
            v[maior] = aux;
        }
    }
}

void imprimirVetorDesordenado(int v[], int n) {
    printf("\nVetor n�o ordenado.\n");
    for (int i = 0; i < n; i++)
        printf("%d\t", v[i]);
}

void imprimirVetorOrdenado(int v[], int n) {
    printf("\nVetor ordenado.\n");
    selectionSort(v, n);
    for (int i = 0; i < n; i++)
        printf("%d\t", v[i]);
}

int main() {

    setlocale(LC_ALL, "Portuguese");
    int vetor1[6] = {2, 1, 4, 3, 6, 5};
    imprimirVetorDesordenado(vetor1, 6);
    imprimirVetorOrdenado(vetor1, 6);

    int vetor2[10] = {2, 1, -4, 3, 6, 7, -8, 10, 9};
    imprimirVetorDesordenado(vetor2, 10);
    imprimirVetorOrdenado(vetor2, 10);

    return 0;
}
